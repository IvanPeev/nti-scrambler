package scrambler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * class to read XML files
 * @author ivan
 */
public class XMLReader {
    private final String ADMIN_FILE = "admins.xml";
    private final String CONNECTIONS_FILE = "connections.xml";

    /**
     * gets the XML document resource pertaining to a specific collection of data
     * such as admin data and connection data
     * @param filename name of the file
     * @return document in the form of an w3c document
     * @throws ParserConfigurationException can be thrown
     * @throws SAXException can be thrown
     * @throws IOException can be thrown
     * @throws FileNotFoundException can be thrown
     */
    private Document getDocument(String filename) throws ParserConfigurationException, FileNotFoundException, SAXException, IOException {
        URL url = this.getClass().getResource("/files/" + filename);
        URLConnection conn = url.openConnection();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(conn.getInputStream());
    }

    /**
     * reads and returns the admin data
     * @return admin data
     * @throws SAXException can be thrown
     * @throws IOException can be thrown
     * @throws FileNotFoundException can be thrown
     */
    public ArrayList<Admin> getAdminsData() throws SAXException, IOException, FileNotFoundException {
        ArrayList<Admin> admins = new ArrayList();
        try {
            Document document = this.getDocument(this.ADMIN_FILE);
            NodeList list = document.getElementsByTagName("admin");

            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String id = element.getElementsByTagName("id").item(0).getTextContent();
                    String email = element.getElementsByTagName("email").item(0).getTextContent();

                    admins.add(new Admin(id, email));
                }
            }
        } catch (ParserConfigurationException ex) {
            throw new Error(ex);
        }
        return admins;
    }

    /**
     * reads and returns the connections data
     * @return connections data
     * @throws SAXException can be thrown
     * @throws IOException can be thrown
     * @throws FileNotFoundException can be thrown
     */
    public ArrayList<ConnectionData> getConnectionsData() throws SAXException, IOException, FileNotFoundException {
        ArrayList<ConnectionData> connections = new ArrayList();

        try {
            Document document = this.getDocument(this.CONNECTIONS_FILE);
            NodeList list = document.getElementsByTagName("connection");

            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String name = element.getElementsByTagName("name").item(0).getTextContent();
                    String host = element.getElementsByTagName("host").item(0).getTextContent();
                    String username = element.getElementsByTagName("username").item(0).getTextContent();
                    String password = element.getElementsByTagName("password").item(0).getTextContent();
                    String database = element.getElementsByTagName("database").item(0).getTextContent();
                    int port = Integer.parseInt(element.getElementsByTagName("port").item(0).getTextContent());

                    connections.add(new ConnectionData(name, host, username, password, database, port));
                }
            }

        } catch (ParserConfigurationException ex) {
            throw new Error(ex);
        }
        return connections;
    }
}
