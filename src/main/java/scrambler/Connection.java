package scrambler;

import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

import javax.xml.parsers.ParserConfigurationException;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.xml.sax.SAXException;

/**
 * class to create connection to Mongo database and do actions
 * @author ivan
 */
public class Connection {
    private MongoClient client = null;
    private MongoDatabase database;

    /**
     * holds value whether currently is connected to database
     * NOTE: used only in corresponding unit test
     */
    public boolean isConnected;

    /**
     * connects the application to a specific connection
     * @param data of the connection
     * @see ConnectionData
     * @throws SAXException can be thrown
     * @throws IOException can be thrown
     * @throws MongoException can be thrown
     */
    public void connect(ConnectionData data) throws SAXException, IOException, MongoException {
        String link = this.createLink(data);
        this.client = MongoClients.create(link);
        this.database = client.getDatabase(data.getDatabase());
        this.isConnected = this.hostIsUp(data);
    }

    /**
     * creates the structure of the URL based on whether the connection
     * needs to be authenticated or not
     * @param data of the connection
     * @see ConnectionData
     * @return a URL string of the connection
     */
    public String createLink(ConnectionData data) {
        String link = "mongodb://";

        if (data.getUsername().isEmpty() && data.getPassword().isEmpty()) {
            // link structure no auth
            // mongodb://host1:port1/?authSource=db1
            link += data.getHost() + ":" + data.getPort();
            link += "/?authSource=" + data.getDatabase();
        } else {
            // link structure with auth
            // mongodb://user1:pwd1@host1:port1/?authSource=db1
            link += data.getUsername() + ":" + data.getPassword() + "@";
            link += data.getHost() + ":" + data.getPort();
            link += "/?authSource=" + data.getDatabase();
        }
        return link;
    }

    /**
     * closes the connection that has been made
     */
    public void closeConnection() {
        this.client.close();
        this.isConnected = false;
    }

    /**
     * checks whether or not the host that the application is attempting
     * to connect to is up
     * @param data of the connection
     * @see ConnectionData
     * @return whether the host is up
     */
    public boolean hostIsUp(ConnectionData data) {
        SocketAddress socketAddress = new InetSocketAddress(data.getHost(), data.getPort());
        Socket socket = new Socket();

        try {
            socket.connect(socketAddress, 1000);
            socket.close();
            return true;
        } catch (SocketTimeoutException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * gets all the Documents from the user collection in the
     * connected to database
     * @return a collection of documents
     */
    public FindIterable<Document> getUsers() {
        MongoCollection<Document> collection = this.database.getCollection("user");
        return collection.find();
    }

    /**
     * scrambles sensitive user data
     *
     * if a user is an admin, then they keep the same email address
     * and the same password
     *
     * if a user is not an admin, then their email address and password are
     * generated based on their index in the user collection in the database
     *
     * example: user who is not an admin with index number 43
     * email: 'test0043@iamprogrez.com'
     * password: TesT3400 (in hashed form)
     * @param user in the form of a BSON Document
     * @param idx of the user in the database collection
     * @param leadingNums the length that the index string should have
     * @return user in the form of a BSON Document
     * @throws MalformedURLException can be thrown
     * @throws ParserConfigurationException can be thrown
     * @throws org.xml.sax.SAXException can be thrown
     */
    public Document scrambleUser(Document user, int idx, int leadingNums) throws MalformedURLException, ParserConfigurationException, SAXException, IOException {
        Scramble scramble = new Scramble();
        Document newUser = user;
        MongoCollection<Document> collection = this.database.getCollection("user");
        boolean isAdmin = !(scramble.getAdminEmail(user).equals(""));
        String userId = newUser.get("_id").toString();

        newUser.put("first_name", scramble.generateName(false));
        newUser.put("last_name", scramble.generateName(true));
        newUser.put("full_address", scramble.generateAddress());
        newUser.put("date_of_birth", scramble.generateDateOfBirth());
        newUser.put("address", scramble.generateRandomString(9));
        newUser.put("city", scramble.generateRandomString(11));
        newUser.put("country", scramble.generateRandomString(15));
        newUser.put("house_number", scramble.generateRandomNumbersString(3));
        newUser.put("nickname", scramble.generateName(false) + " " + scramble.generateName(true));

        if (isAdmin) {
            newUser.put("email", newUser.get("email"));
            newUser.put("password", newUser.get("password"));
        } else {
            String userIdx = Integer.toString(idx);
            String userIdxLeading = scramble.setLeadingZeros(userIdx, leadingNums);
            String userHashedPass = scramble.generateHash(user, userIdxLeading);
            newUser.put("email", "test" + userIdxLeading + "@iamprogrez.com");
            newUser.put("password", userHashedPass);
        }


        collection.replaceOne(eq("_id", new ObjectId(userId)), newUser);
        return collection.find(eq("_id", new ObjectId(userId))).first();
    }

    /**
     * creates a back up of the whole database of the current connection
     * @param data of the connection
     * @see ConnectionData
     * @param outputDirectory to back up the database to
     * @throws IOException can be thrown
     */
    public void backup(ConnectionData data, String outputDirectory) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String command = "mongodump";
        command += " --host \"" + data.getHost() + "\"";
        command += " --port \"" + data.getPort() + "\"";
        command += " --db \"" + data.getDatabase() + "\"";
        command += " --username \"" + data.getUsername() + "\"";
        command += " --password \"" + data.getPassword() + "\"";
        command += " --gzip";
        command += " -o \"" + outputDirectory + "\"";

        rt.exec(command);

        /**
         * used for debugging any problems with backing up
         */
//    Process proc = rt.exec(command);
//    String s;
//    BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//    BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
//
//    // read the output from the command
//    System.out.println("Here is the standard output of the command:\n");
//    while ((s = stdInput.readLine()) != null) {
//        System.out.println(s);
//    }
//
//    // read any errors from the attempted command
//    System.out.println("Here is the standard error of the command (if any):\n");
//    while ((s = stdError.readLine()) != null) {
//        System.out.println(s);
//    }
    }
}

