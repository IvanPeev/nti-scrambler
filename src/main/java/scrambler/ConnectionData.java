package scrambler;

/**
 * structure for data about connections
 * @author ivan
 */
public class ConnectionData {
    private final String NAME;
    private final String HOST;
    private final String USERNAME;
    private final String PASSWORD;
    private final String DATABASE;
    private final int PORT;

    /**
     * standard constructor for ConnectionData
     * @param name of connection
     * @param host of connection
     * @param username of connection
     * @param password of connection
     * @param database of connection
     * @param port of connection
     */
    public ConnectionData(String name, String host, String username, String password, String database, int port) {
        this.NAME = name;
        this.HOST = host;
        this.USERNAME = username;
        this.PASSWORD = password;
        this.DATABASE = database;
        this.PORT = port;
    }

    /**
     * returns name of specific connection
     * @return connection name
     */
    public String getName() {
        return this.NAME;
    }

    /**
     * returns host of specific connection
     * @return connection host
     */
    public String getHost() {
        return this.HOST;
    }

    /**
     * returns username of specific connection
     * @return connection username
     */
    public String getUsername() {
        return this.USERNAME;
    }

    /**
     * returns password of specific connection
     * @return connection password
     */
    public String getPassword() {
        return this.PASSWORD;
    }

    /**
     * returns database of specific connection
     * @return connection database
     */
    public String getDatabase() {
        return this.DATABASE;
    }

    /**
     * returns port of specific connection
     * @return connection port
     */
    public int getPort() {
        return this.PORT;
    }
}