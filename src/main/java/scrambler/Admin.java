package scrambler;

/**
 * structure for admin users
 * @author ivan
 */
public class Admin {
    private final String ID;
    private final String EMAIL;

    /**
    * standard constructor for Admin
    * @param id string form of a Mongo ObjectID
    * @param email of user
    */
    public Admin(String id, String email) {
        this.ID = id;
        this.EMAIL = email;
    }

    /**
    * returns ID of admin
    * @return admin ID
    */
    public String getId() {
        return this.ID;
    }

    /**
    * returns email of admin
    * @return admin email
    */
    public String getEmail() {
        return this.EMAIL;
    }
}
