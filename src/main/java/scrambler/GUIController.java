package scrambler;

import com.mongodb.MongoException;
import com.mongodb.MongoSocketException;
import com.mongodb.client.FindIterable;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import javax.xml.parsers.ParserConfigurationException;

import org.bson.Document;
import org.xml.sax.SAXException;

/**
 * class to control the GUI and handle all actions
 * @author ivan
 */
public class GUIController implements Initializable {

    /**
     * the choice box for the currently available connections
     */
    @FXML public ChoiceBox connectionChoiceBox;

    /**
     * the button to clear the log text
     */
    @FXML public Button clearLogButton;

    /**
     * the button to quit the application
     */
    @FXML public Button quitButton;

    /**
     * the text area where log text is appended
     */
    @FXML public TextArea logTextArea;

    /**
     * the button to scramble the user collection in the database
     */
    @FXML public Button scrambleButton;

    /**
     * the button to back up the whole database
     */
    @FXML public Button backupButton;

    private ArrayList<ConnectionData> connections;
    private ConnectionData selectedConnection;

    /**
     * shows a alert window
     * @param alertType the type of alert
     * @param owner to which window this alert pertains
     * @param title the title of the alert
     * @param message the message of the alert
     */
    private void showAlert(AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    /**
     * shows an alert prompt window
     * @param alertType the type of alert
     * @param title the title of the alert
     * @param message the message of the alert
     * @return the result of the prompt
     */
    private Optional<ButtonType> showPrompt(AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText("");
        alert.setContentText(message);

        return alert.showAndWait();
    }

    /**
     * initializes the application with the necessary data and
     * sets any necessary behavior of the application
     * @param url standard URL
     * @param rb standard ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            XMLReader scheme = new XMLReader();

            this.connections = scheme.getConnectionsData();
            this.connections.forEach((connection) -> {
                this.connectionChoiceBox.getItems().add(connection.getName());
            });
            this.logTextArea.setEditable(false);
            this.logTextArea.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
                logTextArea.setScrollTop(Double.MAX_VALUE);
            });
        } catch (
                SAXException |
                        IOException ex
        ) {
            throw new Error(ex);
        }
    }

    /**
     * appends text to the text area
     * @param text to be appended
     */
    public void appendLogText(String text) {
        this.logTextArea.appendText(text);
    }

    /**
     * starts a task that is going to be executed by a Runnable
     * @param task to be executed
     */
    public void startTask(Runnable task) {
        Thread backgroundThread = new Thread(task);
        backgroundThread.setDaemon(true);
        backgroundThread.start();
    }

    /**
     * handles the behavior when clicked on the Quit button
     */
    public void handleQuitProgram() {
        Optional<ButtonType> result = this.showPrompt(
                AlertType.CONFIRMATION,
                "Quit",
                "Are you sure you want to quit?"
        );

        if (result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    /**
     * scrambles all the users in the user collection of the current connection
     *
     * if no connection is selected then shows an alert window with the necessary
     * message
     *
     * if not able to connect to the host, then appends message to the text area
     *
     * if is able to connect to the database, then scrambles all users using
     * runnable tasks, appends log texts for every user scrambled and closes
     * the connection when done
     * @throws MalformedURLException can be thrown
     * @throws ParserConfigurationException can be thrown
     * @throws InterruptedException can be thrown
     */
    public void handleScramble() throws MalformedURLException, ParserConfigurationException, InterruptedException {
        if (this.connectionChoiceBox.getValue() == null) {
            Window owner = this.scrambleButton.getScene().getWindow();
            this.showAlert(AlertType.ERROR, owner, "Error", "No connection selected");
            return;
        }

        Connection client = new Connection();

        if (client.hostIsUp(this.selectedConnection)) {
            Optional<ButtonType> result = this.showPrompt(
                    AlertType.CONFIRMATION,
                    "Quit",
                    "Are you sure you want to scramble all users in " + this.selectedConnection.getName() + "?"
            );

            if (result.get() == ButtonType.OK) {
                this.backupButton.setDisable(true);
                this.scrambleButton.setDisable(true);
                Runnable task = () -> {
                    try {
                        String link = client.createLink(selectedConnection);
                        appendLogText("Scrambling users at \"" + link + "\"...\n");
                        Thread.sleep(500);
                        client.connect(selectedConnection);

                        FindIterable<Document> foundUsers = client.getUsers();
                        ArrayList<Document> users = new ArrayList();
                        int leadingNums;
                        int lengthNums;

                        for (Document foundUser: foundUsers) {
                            users.add(foundUser);
                        }

                        lengthNums = Integer.toString(users.size()).length();
                        leadingNums = lengthNums > 4 ? lengthNums : 4;

                        for (int i = 0; i < users.size(); i++) {
                            client.scrambleUser(users.get(i), i, leadingNums);
                            appendLogText((i + 1) + " of " + users.size() + " scrambled\n");
                        }

                        appendLogText("Done...\n");
                        client.closeConnection();
                        this.backupButton.setDisable(false);
                        this.scrambleButton.setDisable(false);
                    } catch (SAXException | IOException | MongoSocketException | ParserConfigurationException | InterruptedException ex) {}
                };
                startTask(task);
            }
        } else {
            this.appendLogText("Unable to connect to \"" + this.selectedConnection.getName() + "\"...\n");
        }
    }

    /**
     * creates a back up of the whole database that the application is connected to
     *
     * if no connection is selected then shows an alert window with the necessary message
     *
     * if not able to connect to the host, then appends message to the text area
     *
     * if is able to connect to the database, then prompts the user to select a
     * directory in which to output the database, creates the back up, logs all
     * the necessary information during the connection and lastly closes the
     * connection
     *
     * NOTE: currently this method is not supported on Windows due to the fact
     * that the Java Mongo libraries do not have internal functionality to create
     * a back up, thus for now a back up is created through a CLI command which
     * is supported on MacOS and Linux, but not on Windows because the Mongo
     * executable files are not cross-platform compatible
     * in the future, when it is necessary, support for Windows will be added
     *
     * @throws InterruptedException can be thrown
     */
    public void handleBackup() throws InterruptedException {
        if (this.connectionChoiceBox.getValue() == null) {
            Window owner = this.backupButton.getScene().getWindow();
            this.showAlert(AlertType.ERROR, owner, "Error", "No connection selected");
            return;
        }

        String osName = System.getProperty("os.name");
        Window owner = this.backupButton.getScene().getWindow();
        if (osName.startsWith("Windows")) {
            this.showAlert(AlertType.INFORMATION, owner, "Notice", "Back up not available for Windows");
        } else {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File userHome = new File(System.getProperty("user.home"));
            directoryChooser.setInitialDirectory(userHome);
            File selectedDirectory = directoryChooser.showDialog(owner);

            if (selectedDirectory != null) {
                this.backupButton.setDisable(true);
                this.scrambleButton.setDisable(true);
                Runnable task = () -> {
                    Connection client = new Connection();

                    if (client.hostIsUp(selectedConnection)) {
                        try {
                            client.connect(selectedConnection);
                            appendLogText("Creating back up to " + selectedDirectory + "\n");
                            Thread.sleep(500);
                            client.backup(selectedConnection, selectedDirectory.toString());
                            appendLogText("Done...\n");
                            client.closeConnection();
                        } catch (
                                IOException |
                                        SAXException |
                                        InterruptedException |
                                        MongoException ex
                        ) {}
                    } else {
                        appendLogText("Unable to connect to \"" + selectedConnection.getName() + "\"...\n");
                    }
                    this.backupButton.setDisable(false);
                    this.scrambleButton.setDisable(false);
                };
                startTask(task);
            }
        }
    }

    /**
     * whenever a new connection has been selected, then the currently selected
     * connection variable is newly declared
     */
    public void handleNewSelectedConnection() {
        for (ConnectionData connection: this.connections) {
            if (this.connectionChoiceBox.getValue().toString().equals(connection.getName())) {
                this.selectedConnection = connection;
                break;
            }
        }
    }

    /**
     * clears all text in the text log
     */
    public void handleClearLog() {
        this.logTextArea.setText("");
    }
}

