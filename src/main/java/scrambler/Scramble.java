package scrambler;

import java.io.IOException;
import java.util.ArrayList;

import org.bson.Document;
import org.mindrot.jbcrypt.BCrypt;
import org.xml.sax.SAXException;

/**
 * class to modify the information of a user
 * @author ivan
 */
public class Scramble {
    /**
     * generates a hash of a user's password if they are a non-admin
     * @param user in the form of a BSON Document
     * @param idx of given user
     * @return a hashed password
     */
    public String generateHash(Document user, String idx) {
        String salt = BCrypt.gensalt();
        String password = "TesT" + this.reverseString(idx);
        return BCrypt.hashpw(password, salt);
    }

    /**
     * returns a string with an email address if a given user is an admin
     * otherwise returns an empty string
     * @param user in the form of a BSON Document
     * @return an empty or non-empty string
     * @throws org.xml.sax.SAXException can be thrown
     * @throws java.io.IOException can be thrown
     */
    public String getAdminEmail(Document user) throws SAXException, IOException {
        String userId;
        String userEmail = user.get("email").toString();

        if (user.get("_id").toString() != null) {
            userId = user.get("_id").toString();
        } else {
            userId = user.get("id").toString();
        }

        XMLReader scheme = new XMLReader();
        ArrayList<Admin> admins = scheme.getAdminsData();

        for (Admin admin : admins) {
            String adminId = admin.getId();
            String adminEmail = admin.getEmail();

            if (userId.equals(adminId) && userEmail.equals(adminEmail)) {
                return adminEmail;
            }
        }
        return "";
    }

    /**
     * reverses a string
     * example: 'Test' becomes 'tseT'
     * @param s string to be reversed
     * @return reversed string
     */
    public String reverseString(String s) {
        return new StringBuilder(s).reverse().toString();
    }

    /**
     * sets leading zeros in an index string
     * example: '43' becomes '0043'
     * @param s index number in the form of a string
     * @param len that the string should have
     * @return string with a number of leading zeros
     */
    public String setLeadingZeros(String s, int len) {
        String newString = s;
        while (newString.length() < len) {
            newString = "0" + newString;
        }
        return newString;
    }

    /**
     * generates a name
     * if it is a first name then it is between 3-13 characters long
     * if it is a surname then it is between 6-22 characters long
     * @param surname to signify first name or surname
     * @return a random string
     */
    public String generateName(boolean surname) {
        int randomLength;
        if (surname) {
            randomLength = (int) (Math.random() * 16) + 6;
        } else {
            randomLength = (int) (Math.random() * 10) + 3;
        }
        return this.generateRandomString(randomLength);
    }

    /**
     * generates a random date of birth in the ISO date format
     * @return random date of birth string in the ISO date format
     */
    public String generateDateOfBirth() {
        String date = "";
        date += (int) (Math.floor(Math.random() * 30) + 1960); // year
        date += "-12-"; // month
        date += "31"; // day
        date += "T22:00:00.000Z"; // hour:minute:second:millisecond
        return date;
    }

    /**
     * generates an address
     * @return an address
     */
    public String generateAddress() {
        String address = "";
        address += this.generateRandomString(10); // address name
        address += " " + this.generateRandomNumbersString(2); // address number
        address += ", " + this.generateRandomNumbersString(4); // postal code
        address += " " + this.generateRandomString(7); // city
        address += ", " + this.generateRandomString(9); // country
        return address;
    }

    /**
     * generates a random string of n alphabetical characters
     * @param n number of characters
     * @return string of n random alphabetical characters
     */
    public String generateRandomString(int n) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String randomString = "";

        for (int i = 0; i < n; i++) {
            int randomIdx = (int) (Math.random() * alphabet.length());
            randomString += alphabet.charAt(randomIdx);
        }

        return randomString;
    }

    /**
     * generates a random string of n numerical characters
     * @param n number of characters
     * @return string of n random numerical characters
     */
    public String generateRandomNumbersString(int n) {
        String numerics = "0123456789";
        String randomNumbers = "";

        for (int i = 0; i < n; i++) {
            int randomIdx = (int) (Math.random() * numerics.length());
            randomNumbers += numerics.charAt(randomIdx);
        }

        return randomNumbers;
    }
}
