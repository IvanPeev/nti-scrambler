package scrambler;

import java.io.File;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import net.sourceforge.htmlunit.corejs.javascript.tools.debugger.Main;
import org.apache.commons.io.FileUtils;
import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

/**
 *
 * @author ivan
 */
public class ScramblerAppTest extends ApplicationTest {
    /**
     * use the standard stage for each test
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent mainNode = FXMLLoader.load(Main.class.getResource("/fxml/Scene.fxml"));
        stage.setScene(new Scene(mainNode));
        stage.show();
        stage.toFront();
    }

    /**
     * delete the backed up database if it exists,
     * throw away the current stage after each test is done
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        String outputDirectory = System.getProperty("user.home");
        File backUpDir = new File(outputDirectory + "/iamprogrez");
        if (backUpDir.exists()) {
            FileUtils.deleteDirectory(backUpDir);
        }

        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    /**
     * tests if a back up can be created successfully
     * @throws InterruptedException
     */
    @Test
    public void testBackupSuccessful() throws InterruptedException {
        TextArea textArea = (TextArea) GuiTest.find("#logTextArea");
        String result = "Creating back up to /home/ivan\nDone...\n";
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#backupButton");
        type(KeyCode.ENTER);
        Thread.sleep(2500);
        Assert.assertThat(textArea.getText(), is(result));
    }

    /**
     * tests if a back up can be cancelled when selecting the output directory
     * @throws InterruptedException
     */
    @Test
    public void testBackupCancel() throws InterruptedException {
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#backupButton");
        type(KeyCode.ESCAPE);
        Thread.sleep(2500);
        Label label = (Label) GuiTest.find("Scrambler GUI");
        Assert.assertThat(label.getText(), is("Scrambler GUI"));
    }

    /**
     * testing if back up can be made when no connection is selected
     */
    @Test
    public void testBackupNoConnectionSelected() {
        String result = "No connection selected";
        clickOn("#backupButton");
        Label label = (Label) GuiTest.find("No connection selected");
        Assert.assertThat(label.getText(), is(result));
        Button button = (Button) GuiTest.find("OK");
        clickOn(button);
    }

    /**
     * testing if the host is down of a selected connection when creating a
     * back up
     * @throws InterruptedException
     */
    @Test
    public void testBackupHostIsDown() throws InterruptedException {
        TextArea textArea = (TextArea) GuiTest.find("#logTextArea");
        String result = "Unable to connect to \"Test acceptance database\"...\n";
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#backupButton");
        type(KeyCode.ENTER);
        Thread.sleep(2500);
        Assert.assertThat(textArea.getText(), is(result));
    }

    /**
     * testing if the user collection can be scrambled successfully
     * @throws InterruptedException
     */
    @Test
    public void testScrambleSuccessful() throws InterruptedException {
        TextArea textArea = (TextArea) GuiTest.find("#logTextArea");
        String result = "Scrambling users at ";
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#scrambleButton");

        Button button = GuiTest.find("OK");
        clickOn(button);
        Thread.sleep(100);
        Assert.assertTrue(textArea.getText().contains(result));
    }

    /**
     * testing if the user collection can be scrambled when no connection
     * is selected
     */
    @Test
    public void testScrambleNoConnectionSelected() {
        String result = "No connection selected";
        clickOn("#scrambleButton");
        Label label = (Label) GuiTest.find("No connection selected");
        Assert.assertThat(label.getText(), is(result));
        Button button = (Button) GuiTest.find("OK");
        clickOn(button);
    }

    /**
     * testing if the host is down of a selected connection when trying to
     * scramble the user collection
     * @throws InterruptedException
     */
    @Test
    public void testScrambleHostIsDown() throws InterruptedException {
        TextArea textArea = (TextArea) GuiTest.find("#logTextArea");
        String result = "Unable to connect to \"Test acceptance database\"...\n";
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#scrambleButton");
        type(KeyCode.ENTER);
        Thread.sleep(2500);
        Assert.assertThat(textArea.getText(), is(result));
    }

    /**
     * tests whether the text log area has been cleared
     * @throws InterruptedException
     */
    @Test
    public void testClearLog() throws InterruptedException {
        TextArea textArea = (TextArea) GuiTest.find("#logTextArea");
        String result = "Unable to connect to \"Test acceptance database\"...\n";
        clickOn("#connectionChoiceBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#backupButton");
        type(KeyCode.ENTER);
        Thread.sleep(2500);
        Assert.assertThat(textArea.getText(), is(result));

        Button clearButton = (Button) GuiTest.find("Clear");
        clickOn(clearButton);
        Assert.assertThat(textArea.getText(), is(""));
    }

    /**
     * tests whether quitting the application has been cancelled successfully
     */
    @Test
    public void testQuitCancel() {
        Button quitButton = (Button) GuiTest.find("#quitButton");
        clickOn(quitButton);
        Button cancelButton = (Button) GuiTest.find("Cancel");
        clickOn(cancelButton);
        Label label = (Label) GuiTest.find("Scrambler GUI");
        Assert.assertThat(label.getText(), is("Scrambler GUI"));
    }

    /**
     * NOTE: no test for handleQuit
     *
     * creating a test to see if the application is quit successfully is not
     * possible because the application needs to be running in order to run a
     * test, if the application is quit while expecting to run tests then the
     * testing will be unsuccessful
     *
     * handleQuit in the GUIController simply executes Platform.exit()
     */
}

