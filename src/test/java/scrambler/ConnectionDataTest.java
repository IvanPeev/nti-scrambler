package scrambler;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ivan
 */
public class ConnectionDataTest {
    ConnectionData instance;

    /**
     * use the same connection data for each test
     */
    @Before
    public void setUp() {
        instance = new ConnectionData("Local acceptance database", "localhost", "", "", "iamprogrez", 27017);
    }

    /**
     * Test of getName method, of class ConnectionData.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Local acceptance database";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHost method, of class ConnectionData.
     */
    @Test
    public void testGetHost() {
        System.out.println("getHost");
        String expResult = "localhost";
        String result = instance.getHost();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUsername method, of class ConnectionData.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        String expResult = "";
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassword method, of class ConnectionData.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        String expResult = "";
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDatabase method, of class ConnectionData.
     */
    @Test
    public void testGetDatabase() {
        System.out.println("getDatabase");
        String expResult = "iamprogrez";
        String result = instance.getDatabase();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPort method, of class ConnectionData.
     */
    @Test
    public void testGetPort() {
        System.out.println("getPort");
        int expResult = 27017;
        int result = instance.getPort();
        assertEquals(expResult, result);
    }
}
