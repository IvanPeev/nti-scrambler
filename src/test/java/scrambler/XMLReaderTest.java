package scrambler;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ivan
 */
public class XMLReaderTest {
    XMLReader instance;

    /**
     * use the standard XMLReader for each test
     */
    @Before
    public void setUp() {
        instance = new XMLReader();
    }

    /**
     * Test of getAdmins
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAdminsData() throws Exception {
        System.out.println("getAdminsData");

        ArrayList<Admin> expResult = instance.getAdminsData();
        ArrayList<Admin> result = instance.getAdminsData();
        assertEquals(expResult.get(0).getId(), result.get(0).getId());
    }

    /**
     * Test of getConnectionsData method, of class XMLReader.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetConnectionsData() throws Exception {
        System.out.println("getConnectionsData");

        ArrayList<ConnectionData> expResult = instance.getConnectionsData();
        ArrayList<ConnectionData> result = instance.getConnectionsData();
        assertEquals(expResult.get(0).getName(), result.get(0).getName());
    }
}
