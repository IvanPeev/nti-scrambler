package scrambler;

import com.mongodb.client.FindIterable;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author ivan
 */
public class ConnectionTest {
    ConnectionData data;
    Connection instance;

    /**
     * use the same connection data for each test
     */
    @Before
    public void setUp() {
        data = new ConnectionData("Local acceptance database", "localhost", "", "", "iamprogrez", 27017);
        instance = new Connection();
    }

    /**
     * delete the backed up database if it exists
     * @throws IOException
     */
    @After
    public void tearDown() throws IOException {
        String outputDirectory = System.getProperty("user.home");
        File backUpDir = new File(outputDirectory + "/iamprogrez");
        if (backUpDir.exists()) {
            FileUtils.deleteDirectory(backUpDir);
        }
    }

    /**
     * Test of connect method, of class Connection.
     * @throws java.lang.Exception
     */
    @Test
    public void testConnect() throws Exception {
        System.out.println("connect");
        instance.connect(data);
        assertTrue(instance.isConnected);
    }

    /**
     * Test of createLink method, of class Connection.
     */
    @Test
    public void testCreateLink() {
        System.out.println("createLink");
        String expResult = "mongodb://localhost:27017/?authSource=iamprogrez";
        String result = instance.createLink(data);
        assertEquals(expResult, result);
    }

    /**
     * Test of closeConnection method, of class Connection.
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    @Test
    public void testCloseConnection() throws SAXException, IOException {
        System.out.println("closeConnection");
        instance.connect(data);
        assertTrue(instance.isConnected);
        instance.closeConnection();
        assertFalse(instance.isConnected);
    }

    /**
     * Test of hostIsUp method, of class Connection.
     */
    @Test
    public void testHostIsUp() {
        System.out.println("hostIsUp");
        boolean expResult;
        boolean result;

        expResult = true;
        result = instance.hostIsUp(data);
        assertEquals(expResult, result);

        data = new ConnectionData("Local acceptance database", "localhost", "", "", "iamprogrez", 27108);
        expResult = false;
        result = instance.hostIsUp(data);
        assertEquals(expResult, result);

        data = new ConnectionData("Local acceptance database", "localhos", "", "", "iamprogrez", 27107);
        expResult = false;
        result = instance.hostIsUp(data);
        assertEquals(expResult, result);

        data = new ConnectionData("Local acceptance database", "localhos", "", "", "iamprogrez", 27108);
        expResult = false;
        result = instance.hostIsUp(data);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUsers method, of class Connection.
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    @Test
    public void testGetUsers() throws SAXException, IOException {
        System.out.println("getUsers");
        instance.connect(data);
        FindIterable<Document> result = instance.getUsers();
        System.out.println(result);

        assertTrue(result != null);
    }

    /**
     * Test of scrambleUser method, of class Connection.
     * @throws java.lang.Exception
     */
    @Test
    public void testScrambleUser() throws Exception {
        System.out.println("scrambleUser");
        instance.connect(data);
        Document user = instance.getUsers().first();
        System.out.println(user.get("first_name"));
        int idx = 0;
        int leadingNums = 4;
        String name = user.get("first_name").toString();

        Document newUser = instance.scrambleUser(user, idx, leadingNums);
        String newName = newUser.get("first_name").toString();

        assertFalse(name.equals(newName));
    }

    /**
     * Test of backup method, of class Connection.
     * Note: test does create an actual back up in the user's home directory
     * @throws java.lang.Exception
     */
    @Test
    public void testBackup() throws Exception {
        System.out.println("backup");
        String outputDirectory = System.getProperty("user.home");
        instance.backup(data, outputDirectory);

        Thread.sleep(1000);

        File file = new File(outputDirectory + "/iamprogrez");
        assertTrue(file.exists());
    }
}

