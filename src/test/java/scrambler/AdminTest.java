package scrambler;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ivan
 */
public class AdminTest {
    Admin instance;

    /**
     * use the same user for each test
     */
    @Before
    public void setUp() {
        instance = new Admin("57c9450c841ba00010fe2c52", "i.ivanov.peev@hotmail.com");
    }

    /**
     * Test of getId method, of class Admin.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        String expResult = "57c9450c841ba00010fe2c52";
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Admin.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = "i.ivanov.peev@hotmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }
}
