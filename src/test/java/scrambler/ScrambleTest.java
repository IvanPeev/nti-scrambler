package scrambler;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.xml.parsers.ParserConfigurationException;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author ivan
 */
public class ScrambleTest {
    Scramble instance;
    Document user;
    Document testUser;

    /**
     * use a standard admin and non-admin user for each test
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     */
    @Before
    public void setUp() throws MalformedURLException, ParserConfigurationException {
        instance = new Scramble();
        user = new Document();
        user.append("_id", "57c9450c841ba00010fe2c52");
        user.append("id", "57c9450c841ba00010fe2c52");
        user.append("email", "i.ivanov.peev@hotmail.com");
        user.append("password", "welcome123");

        testUser = new Document();
        testUser.append("_id", "55dd5fc520b0bc1200a6a880");
        testUser.append("id", "55dd5fc520b0bc1200a6a880");
        testUser.append("email", "test0034@iamprogrez.com");
        testUser.append("password", "S#aha3hbA$");
    }

    /**
     * Test of generateHash method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateHash() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateHash");
        String num = "0034";
        String result = instance.generateHash(testUser, num);
        System.out.println(result);
        assertTrue(result instanceof String);
        assertTrue(result.length() > testUser.get("password").toString().length());
        assertTrue(result.length() == 60);
    }

    /**
     * Test of getAdmin method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    @Test
    public void testGetAdminEmail() throws MalformedURLException, ParserConfigurationException, SAXException, IOException {
        System.out.println("getAdminEmail");
        String result = instance.getAdminEmail(user);
        assertEquals(user.get("email"), result);
    }

    /**
     * Test of reverseString method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testReverseString() throws MalformedURLException, ParserConfigurationException {
        System.out.println("reverseString");
        String s = "Test text";
        String expResult = "txet tseT";
        String result = instance.reverseString(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of setLeadingZeros method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testSetLeadingZeros() throws MalformedURLException, ParserConfigurationException {
        System.out.println("setLeadingZeros");
        String s = "34";
        int len = 4;
        String expResult = "0034";
        String result = instance.setLeadingZeros(s, len);
        assertEquals(expResult, result);
    }

    /**
     * Test of generateName method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateName() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateName");
        String firstNameResult = instance.generateName(false);
        String surnameResult = instance.generateName(true);
        assertTrue(firstNameResult instanceof String);
        assertTrue(surnameResult instanceof String);
        assertTrue((firstNameResult.length() <= surnameResult.length()) || (firstNameResult.length() > surnameResult.length()));
    }

    /**
     * Test of generateDateOfBirth method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateDateOfBirth() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateDateOfBirth");
        String result = instance.generateDateOfBirth();
        assertTrue(result.length() == 24);
        assertTrue(result instanceof String);
    }

    /**
     * Test of generateAddress method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateAddress() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateAddress");
        String result = instance.generateAddress();
        assertTrue(result.length() == 38);
        assertTrue(result instanceof String);
    }

    /**
     * Test of generateRandomString method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateRandomString() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateRandomString");
        int n = 7;
        String result = instance.generateRandomString(n);
        assertTrue(result.length() == 7);
        assertTrue(result instanceof String);
    }

    /**
     * Test of generateRandomNumbersString method, of class Scramble.
     * @throws java.net.MalformedURLException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    @Test
    public void testGenerateRandomNumbersString() throws MalformedURLException, ParserConfigurationException {
        System.out.println("generateRandomNumbersString");
        int n = 5;
        String result = instance.generateRandomNumbersString(n);
        assertTrue(result.length() == 5);
        assertTrue(result instanceof String);
    }
}
